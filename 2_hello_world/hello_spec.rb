require_relative 'hello'

describe 'greeted' do
  it 'returns greeted with the name' do
    expect(hello('Alina')).to eq 'Hello Alina'
  end
end
