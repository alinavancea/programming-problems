def hello(name)
  raise ArgumentError unless %w(Alice Bob).include?(name)
  "Hello #{name}"
end
