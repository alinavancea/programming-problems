require_relative 'hello'
# Source: http://stackoverflow.com/questions/19960831/rspec-expect-vs-expect-with-block-whats-the-difference
# Notes
# expect('test').to eq('test')
# simple testing the returing value if you try the same with raising error

# expect(raise "error").to raise_error

# The test will fail right here when the code is executed, because the argument is imedialtly evaluated
# When it is used a block, from ruby, the block isn't executed immediately
# it's execution is determined by the method you're calling (in this case, the expect method handles when to execute your block):

# expect{raise "fail!"}.to raise_error

# We can look at an example method that might handle this behavior:

# def expect(val=nil)
#   if block_given?
#     begin
#       yield
#     rescue
#       puts "Your block raised an error!"
#     end
#   else
#     puts "The value under test is #{val}"
#   end
# end

# You can see here that it's the expect method that is manually rescuing your error so that it can test whether or not
# errors are raised, etc. yield is a ruby method's way of executing whatever block was passed to the method.
describe 'greeted' do
  it 'returns greeted with Alice' do
    expect(hello('Alice')).to eq 'Hello Alice'
  end
  it 'returns greeted with Bob' do
    expect(hello('Bob')).to eq 'Hello Bob'
  end

  it 'raise ArgumentError with Alina' do
    expect{ hello('Alina') }.to raise_error ArgumentError
  end
  it 'raise ArgumentError with nil' do
    expect{ hello(nil) }.to raise_error ArgumentError
  end
end
