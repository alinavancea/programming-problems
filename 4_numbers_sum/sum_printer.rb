# Adding benchmark
require_relative 'sum'
require 'benchmark'

number = STDIN.gets

puts "Formula #{sum(number)}"
puts Benchmark.measure { sum(number) }

puts "Range #{sum_range(number)}"
puts Benchmark.measure { sum_range(number) }

puts "Array #{sum_range(number)}"
puts Benchmark.measure { sum_range(number) }

