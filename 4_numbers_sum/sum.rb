# Using math formula
def sum(number)
  number = number.to_i
  number * (number + 1) / 2
end

# Using a ruby range
# we start with 0 for the case of nil
def sum_range(number)
  (0..number.to_i).inject(:+)
end

# Using a ruby array
# we start with 0 for the case of nil
def sum_array(number)
  (number.to_i + 1).times.inject(:+)
end

