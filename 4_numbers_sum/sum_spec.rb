require_relative 'sum'

describe 'sum' do
  it 'returns sum 55' do
    expect(sum(10)).to eq 55
  end

  it 'returns sum 55 when string is sent' do
    expect(sum('10')).to eq 55
  end

  it 'returns 0' do
    expect(sum(0)).to eq 0
  end
end

describe 'sum_range' do
  it 'returns sum 55' do
    expect(sum_range(10)).to eq 55
  end

  it 'returns sum 55 when string is sent' do
    expect(sum_range('10')).to eq 55
  end

  it 'returns 0' do
    expect(sum_range(0)).to eq 0
  end
end

describe 'sum_range' do
  it 'returns sum 55' do
    expect(sum_array(10)).to eq 55
  end

  it 'returns sum 55 when string is sent' do
    expect(sum_array('10')).to eq 55
  end

  it 'returns 0' do
    expect(sum_array(0)).to eq 0
  end
end
